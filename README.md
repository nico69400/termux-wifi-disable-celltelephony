FR
-

But de l'opération?
-

Désactiver le wifi lorsque l'on quitte son domicile en se basant sur les antennes radio présentes mais aussi le réactiver une fois de retour.


Pour celà, commencer par installer termux et termux-api à partir de F-droid (https://f-droid.org/en/packages/com.termux/)


lancer termux puis la première chose à faire est de mettre à jour les dépots puis installer l'api:


`$>apt update && apt upgrade`

`$>pkg install termux-api`


Pour que le script fonctionne il faut connaitre les différents ID des antennes présentes autour. On va donc commencer par lancer la commande:

`$>termux-telephony-cellinfo`

Rechercher les valeurs pour "ci" : (lte) et "cid" :(GSM)
Placer ces valeurs dans le script Wifi.check (à la place de XXXX YYYY ZZZZ). Réduire le nombre de pipe "|" si 1 antenne

Copier le script dans votre répertoire courant sous le dossier caché shortcuts

`~/.shortcuts/Wifi.check`

Pour aller plus loin, vous pouvez creer un cron afin d'executer la commande

Pour cela 2 solutions:

`$>cd`

`$>nano ../usr/var/spool/cron/crontabs/u0XXXX` (ici le nom de votre user par défaut)

ou

`$>crontab -e` (attention en mode vi donc "i" pour insertion et "Esc : wq!" pour quitter et sauvegarder

exemple:

`*/5 7-22 * * * ~/.shortcuts/Wifi.check 2> ~/wifi.log`  <== Executer de 7h à 22h toutes les 5 minutes et redirection d'erreur dans le fichier de log

`$>crond` <== Active le daemon cron

Si vous voulez que le cron soit actif penser à créer un .bash_profile

`$>cd ~`

`$>nano .bash_profile`

insérer la ligne: `crond` et Ctrl+x


EN (thank you [deepl.com](deepl.com))
-



Purpose of the operation?
-

Disable wifi when leaving home based on the radio antennas present but also reactivate it once back home.

To do this, start by installing termux and termux-api from F-droid (https://f-droid.org/en/packages/com.termux/)

launch termux then the first thing to do is to update the repositories:

`$>apt update && apt upgrade`

For the script to work, you need to know the different IDs of the antennas around it. So we'll start by running the command:

`$>terminal-telephony-cellinfo`

Search for values for "ci": (lte) and "cid":(GSM)

Place these values in the Wifi.check script (instead of XXXX YYYYY ZZZZ). Reduce the number of pipes "|" if 1 antenna.

Copy the script into your current directory under the hidden folder shortcuts

`~/.shortcuts/Wifi.check`

To go further, you can create a cron to execute the command.

For this purpose, there are 2 solutions:

`$>cd`

`$>nano ../usr/var/spool/spool/cron/crontabs/u0XXXX` (here the name of your default user)

or

`crontab -e` (be careful in vi mode so "i" for insertion and "Esc: wq!" to exit and save

example:

`*/5 7-22 * * * * ~/.shortcuts/Wifi.check 2> ~/wifi.log` <=== Run from 7am to 10pm every 5 minutes and error forwarding in the log file

`$>crond` <== Enable daemon cron

If you want that cron daemon is actived on termux start please create .bash_profile and add a line crond
`$>cd ~`

`$>nano .bash_profile`

Insert line : `crond` and Ctrl+x